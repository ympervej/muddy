function findBaseName(url) {
    var fileName = url.substring(url.lastIndexOf('/') + 1);
    var dot = fileName.lastIndexOf('.');
    return dot == -1 ? fileName : fileName.substring(0, dot);
}


;(function($) {

    var $body = $('body'),
        $home_banner = $body.find("#home_banner");

    $body.on('click', 'a[href="#"]', function (e) {
        e.preventDefault();
    });

    function home_banner_full_height() {
        var height = $(window).height(),
            $elem = $home_banner.find('.container'),
            padding_top;
        padding_top = (height - $elem.outerHeight()) / 2;
        $home_banner.css('height', height+'px');
        $home_banner.css('padding-top', padding_top + 'px');
        console.log(height);
    }
    home_banner_full_height();

    $(window).resize(function () {
        home_banner_full_height();
    });


    $body.on('click', '.open_nav', function (e) {
        e.preventDefault();
        if($body.hasClass('blocked')){
            close_nav();
        } else {
            open_nav();
        }

    }).on('click', '.closebtn, .blocker', function (e) {
        e.preventDefault();
        close_nav();
    });

    function open_nav() {
        // $("#mobile_nav").css('width', '250px');
        var $mobile_nav = $( "#mobile_nav" );
        $mobile_nav.animate({
            width: "250px"
        }, 400);
        setTimeout(function () {
            $("#wrapper").addClass('open_navs');
        }, 100);
        $body.addClass('blocked');
    }
    function close_nav($time) {
        // $("#mobile_nav").css('width', '0');
        $( "#mobile_nav" ).animate({
            width: "0"
        }, 400);
        $("#wrapper").removeClass('open_navs');
        $body.removeClass('blocked');
    }

    $body.on('click', 'a[href*="#"]:not([href="#"])', function(e) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                // e.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                // window.location.hash = this.hash;
                if($body.hasClass('blocked')){
                    close_nav();
                }
                return false;
            }
        }
    });


})(jQuery);
